package com.sonnv2412.demospringsecurity.controller;

import com.sonnv2412.demospringsecurity.configuration.annotation.TrackTime;
import com.sonnv2412.demospringsecurity.domain.entity.AccountTransactions;
import com.sonnv2412.demospringsecurity.domain.entity.Customer;
import com.sonnv2412.demospringsecurity.domain.repository.AccountTransactionsRepository;
import com.sonnv2412.demospringsecurity.domain.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class BalanceController {

    @Autowired
    private AccountTransactionsRepository accountTransactionsRepository;
    @Autowired
    private CustomerRepository customerRepository;

    @GetMapping("/myBalance")
    @TrackTime
    public List<AccountTransactions> getBalanceDetails(@RequestParam String email) {
        List<Customer> customers = customerRepository.findByEmail(email);
        if(customers != null && !customers.isEmpty()){
            Customer c = customers.get(0);
            List<AccountTransactions> accountTransactions = accountTransactionsRepository.
                    findByCustomerIdOrderByTransactionDtDesc(c.getId());
            if (accountTransactions != null ) {
                return accountTransactions;
            }
        }
        return null;
    }
}
