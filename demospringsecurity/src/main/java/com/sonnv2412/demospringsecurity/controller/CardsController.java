package com.sonnv2412.demospringsecurity.controller;

import com.sonnv2412.demospringsecurity.domain.entity.Cards;
import com.sonnv2412.demospringsecurity.domain.entity.Customer;
import com.sonnv2412.demospringsecurity.domain.repository.CardsRepository;
import com.sonnv2412.demospringsecurity.domain.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class CardsController {

    @Autowired
    private CardsRepository cardsRepository;
    @Autowired
    private CustomerRepository customerRepository;
    @GetMapping("/myCards")
    public List<Cards> getCardDetails(@RequestParam String email) {
        List<Customer> customers = customerRepository.findByEmail(email);
        if(customers != null && !customers.isEmpty()){
            Customer c = customers.get(0);
            List<Cards> cards = cardsRepository.findByCustomerId(c.getId());
            if (cards != null ) {
                return cards;
            }
        }
        return null;
    }

}
