package com.sonnv2412.demospringsecurity.controller;

import com.sonnv2412.demospringsecurity.domain.entity.Customer;
import com.sonnv2412.demospringsecurity.domain.entity.Loans;
import com.sonnv2412.demospringsecurity.domain.repository.CustomerRepository;
import com.sonnv2412.demospringsecurity.domain.repository.LoanRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class LoansController {

    @Autowired
    private LoanRepository loanRepository;
    @Autowired
    private CustomerRepository customerRepository;

    @GetMapping("/myLoans")
    public List<Loans> getLoanDetails(@RequestParam String email) {
        List<Customer> customers = customerRepository.findByEmail(email);
        if (customers != null && !customers.isEmpty()) {
            Customer c = customers.get(0);
            List<Loans> loans = loanRepository.findByCustomerIdOrderByStartDtDesc(c.getId());
            if (loans != null) {
                return loans;
            }
        }
        return null;
    }
}
