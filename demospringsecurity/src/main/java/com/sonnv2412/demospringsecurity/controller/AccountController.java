package com.sonnv2412.demospringsecurity.controller;

import com.sonnv2412.demospringsecurity.domain.entity.Accounts;
import com.sonnv2412.demospringsecurity.domain.entity.Customer;
import com.sonnv2412.demospringsecurity.domain.repository.AccountsRepository;
import com.sonnv2412.demospringsecurity.domain.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController()
public class AccountController {

    @Autowired
    private AccountsRepository accountsRepository;
    @Autowired
    private CustomerRepository customerRepository;
    @GetMapping("/myAccount")
    public Accounts getAccountDetails(@RequestParam String email) {
        List<Customer> customers = customerRepository.findByEmail(email);
        if(customers != null && !customers.isEmpty()){
            Customer c = customers.get(0);
            Accounts accounts = accountsRepository.findByCustomerId(c.getId());
            if (accounts != null ) {
                return accounts;
            }
        }
        return null;
    }
}
