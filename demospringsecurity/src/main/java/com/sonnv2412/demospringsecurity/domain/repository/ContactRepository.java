package com.sonnv2412.demospringsecurity.domain.repository;

import com.sonnv2412.demospringsecurity.domain.entity.Contact;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface ContactRepository extends CrudRepository<Contact, Long> {
	
	
}
