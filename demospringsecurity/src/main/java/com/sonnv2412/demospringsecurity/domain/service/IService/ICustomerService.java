package com.sonnv2412.demospringsecurity.domain.service.IService;

import com.sonnv2412.demospringsecurity.domain.entity.Customer;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.function.Predicate;

public interface ICustomerService {
    public Customer getCustomerByEmail(String email);
    public List<Customer> getAllCustomers();
    public Customer addCustomer(Customer c);
}
