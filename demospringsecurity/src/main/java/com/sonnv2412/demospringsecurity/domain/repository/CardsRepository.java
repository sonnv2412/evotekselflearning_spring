package com.sonnv2412.demospringsecurity.domain.repository;

import java.util.List;

import com.sonnv2412.demospringsecurity.domain.entity.Cards;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface CardsRepository extends CrudRepository<Cards, Long> {
	
	List<Cards> findByCustomerId(int customerId);

}
