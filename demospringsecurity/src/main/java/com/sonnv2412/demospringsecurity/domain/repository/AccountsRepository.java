package com.sonnv2412.demospringsecurity.domain.repository;

import com.sonnv2412.demospringsecurity.domain.entity.Accounts;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountsRepository extends CrudRepository<Accounts, Long> {
	Accounts findByCustomerId(int customerId);

}
