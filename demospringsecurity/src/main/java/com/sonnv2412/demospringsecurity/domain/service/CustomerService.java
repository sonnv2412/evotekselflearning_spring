package com.sonnv2412.demospringsecurity.domain.service;

import com.sonnv2412.demospringsecurity.configuration.annotation.TrackTime;
import com.sonnv2412.demospringsecurity.domain.entity.Customer;
import com.sonnv2412.demospringsecurity.domain.repository.CustomerRepository;
import com.sonnv2412.demospringsecurity.domain.service.IService.ICustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class CustomerService implements ICustomerService {
    @Autowired
    CustomerRepository customerRepository;
    @Override
    public Customer getCustomerByEmail(String email) {
        List<Customer> customers = customerRepository.findByEmail(email);
        if(customers.size() == 0){
            return null;
        }
        return customerRepository.findByEmail(email).get(0);
    }

    @Override
    public List<Customer> getAllCustomers() {
        return customerRepository.findAll();
    }

    @Override
    @Transactional
    public Customer addCustomer(Customer c) {
        c =  customerRepository.saveAndFlush(c);
       return c;
    }
}
