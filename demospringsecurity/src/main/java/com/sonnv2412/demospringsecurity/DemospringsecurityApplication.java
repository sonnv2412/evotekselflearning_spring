package com.sonnv2412.demospringsecurity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

@SpringBootApplication
@EnableMethodSecurity(prePostEnabled = true)
public class DemospringsecurityApplication {
    public static void main(String[] args) {
		SpringApplication.run(DemospringsecurityApplication.class, args);
    }
}
