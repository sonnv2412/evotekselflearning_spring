package com.sonnv2412.demospringsecurity.configuration.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class TrackTimeAspect {
    private static final Logger logger = LoggerFactory.getLogger(TrackTimeAspect.class);

    @Around("@annotation(com.sonnv2412.demospringsecurity.configuration.annotation.TrackTime)")
    public Object logExecutionTime(ProceedingJoinPoint joinPoint) throws Throwable{
        long start = System.currentTimeMillis();
        Object proceed = joinPoint.proceed();
        long end = System.currentTimeMillis();
        long executionTime = end - start;
        logger.info("{} execute in {} ms", joinPoint.getSignature(), executionTime);
        return joinPoint.proceed();
    }
}
