package com.sonnv2412.demospringsecurity.configuration;

import com.sonnv2412.demospringsecurity.domain.entity.Authority;
import com.sonnv2412.demospringsecurity.domain.entity.Customer;
import com.sonnv2412.demospringsecurity.domain.service.IService.ICustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class BankUsernamePasswordAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    private ICustomerService customerService;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String email = authentication.getName();
        String pwd = authentication.getCredentials().toString();
        Customer customer = customerService.getCustomerByEmail(email);
        if(customer != null){
            if(passwordEncoder.matches(pwd, customer.getPwd())){
                return new UsernamePasswordAuthenticationToken(email, pwd, getGrantedAuthority(customer.getAuthorities()));
            }else{
                throw new BadCredentialsException("Invalid password provided!");
            }
        }else{
            throw new BadCredentialsException("User with email \'" + email + "\' does not exist!");
        }
    }

    public List<GrantedAuthority> getGrantedAuthority(Set<Authority> authoritySet){
        List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
        for(var item : authoritySet){
            GrantedAuthority authority = new SimpleGrantedAuthority(item.getName());
            grantedAuthorities.add(authority);
        }
        return grantedAuthorities;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication);
    }
}
