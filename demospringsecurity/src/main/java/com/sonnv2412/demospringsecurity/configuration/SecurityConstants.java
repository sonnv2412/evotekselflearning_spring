package com.sonnv2412.demospringsecurity.configuration;

public interface SecurityConstants {
    public final static String JWT_KEY = "xXoxZrkcNxTMjKj2uIdQOd7lfL5M4A7pj3S33GWwFW6A9YrD1pVC";
    public final static String JWT_HEADER = "Authorization";
}
