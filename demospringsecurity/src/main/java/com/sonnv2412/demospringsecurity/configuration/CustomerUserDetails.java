package com.sonnv2412.demospringsecurity.configuration;

import com.sonnv2412.demospringsecurity.domain.entity.Customer;
import com.sonnv2412.demospringsecurity.domain.service.IService.ICustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

public class CustomerUserDetails implements UserDetailsService {
    @Autowired
    ICustomerService customerService;
    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        String username, password = null;
        List<GrantedAuthority> authorities = new ArrayList<>();
        Customer user = customerService.getCustomerByEmail(email);
        if(user == null){
            throw new UsernameNotFoundException( email + "not found!");
        }else{
            username = user.getEmail();
            password = user.getPwd();
            authorities.add(new SimpleGrantedAuthority(user.getRole()));
        }
        return new User(username, password, authorities);
    }
}
